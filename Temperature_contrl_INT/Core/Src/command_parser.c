#include "command_parser.h"
#include "uart_driver.h"
#include "main.h"
#include <string.h>

#define  FW_VERSION		   "*V1.0.20220617#"
#define  PROTOCOL_PREAMBLET "*T"
#define  PROTOCOL_POSAMBLET '#'
extern uart_driver_t uart_driver;
extern UART_HandleTypeDef huart1;

char state = 0;
char state1 = 0;

extern UART_HandleTypeDef huart3;

/// Command messages ///
const uint8_t read_temperature_cmd[] 	= "READ_TEMPERATURE";
const uint8_t read_fan_speed_cmd[] 		= "READ_FAN_SPEED";
const uint8_t set_fan_speed_cmd[] 		= "SET_FAN_SPEED";
const uint8_t close_door_cmd[] 			= "CLOSE_DOOR";
const uint8_t open_door_cmd[] 			= "OPEN_DOOR";
const uint8_t get_door_position_cmd[] 	= "GET_DOOR_POSITION";
const uint8_t get_fw_version_cmd[] 		= "GET_FW_VERSION";
const uint8_t get_unit_tick_cmd[] 		= "GET_UNIT_TICK";
const uint8_t get_heater_state_cmd[] 	= "GET_HEATER_STATE";
const uint8_t ack_message[] 			=  "*ACK#";
const uint8_t nack_message[] 			= "*NACK#";
const uint8_t temp_probe1[] 			    = "*T20.31#*D0#*H0#*F025#";
const uint8_t temp_probe [] 			    = "*T20.31#";
const uint8_t update_request_cmd[] = "update_request";

const uint8_t Zero_per[] 		 =  "*F000#\r\n";
const uint8_t twenty_five_per[]  =  "*F025#\r\n";
const uint8_t fifty_per[] 		 =  "*F050#\r\n";
const uint8_t seventy_five_per[] =  "*F075#\r\n";
const uint8_t one_hundred_per[]  =  "*F100#\r\n";
uint32_t  tick = 0;


void parse_command(uint8_t *rx_packet)
{
	if(memcmp(rx_packet, read_temperature_cmd, sizeof (read_temperature_cmd)-1)==0){
	// Read the temperature value

	///problems woth the read of the temperature meas.
//		char buffer[5];
//		uart_driver_send(&uart_driver,(uint8_t *) PROTOCOL_PREAMBLET, sizeof(PROTOCOL_PREAMBLET)-1);
//	    float temp = return_temp();
//		sprintf(buffer,"%.2f",temp);
//		uart_driver_send(&uart_driver,(uint8_t *) read_temperature_cmd , sizeof( read_temperature_cmd)-1);

		uart_driver_send(&uart_driver,(uint8_t *)temp_probe , sizeof( temp_probe )-1);

	}else if (memcmp(rx_packet, read_fan_speed_cmd, sizeof (read_fan_speed_cmd)-1)==0){
	// read the value of the speed of the fan

		switch(state1){
		case 0:
			uart_driver_send(&uart_driver,(uint8_t *)Zero_per, sizeof(Zero_per)-1);
			break;
		case 1:
			uart_driver_send(&uart_driver,(uint8_t *)twenty_five_per, sizeof(twenty_five_per)-1);
			break;
		case 2:
			uart_driver_send(&uart_driver,(uint8_t *)fifty_per, sizeof(fifty_per)-1);
			break;
		case 3:
			uart_driver_send(&uart_driver,(uint8_t *)seventy_five_per, sizeof(seventy_five_per)-1);
			break;
		case 4:
			uart_driver_send(&uart_driver,(uint8_t *)one_hundred_per, sizeof(one_hundred_per)-1);
			state1 = 0;
			break;
		default:
			break;

		}

	}else if (memcmp(rx_packet, set_fan_speed_cmd, sizeof (set_fan_speed_cmd)-1)==0){
		// Set the value of the set of the fan

		switch (state){
		case 0:				// 0%
			PWM_DMA(0.0, 0.0);
			state = state + 1;
			state1 = state1 + 1;
			break;
		case 1: 			// 25%
			PWM_DMA(0.0, 0.25);
			state = state + 1;
			state1 = state1 + 1;
			break;
		case 2: 			// 50%
			PWM_DMA(0.25, 0.50);
			state = state + 1;
			state1 = state1 + 1;
			break;
		case 3:				// 75%
			PWM_DMA(0.50, 0.75);
			state = state + 1;
			state1 = state1 + 1;
			break;
		case 4:				// 100%
			PWM_DMA(0.75, 1);
			state = state + 1;
			state1 = state1 + 1;
			break;
		case 5:
			state  = 0;
			break;
		default:
			break;
		}
		uart_driver_send(&uart_driver,(uint8_t *)ack_message, sizeof(ack_message)-1);

	}else if (memcmp(rx_packet, close_door_cmd, sizeof (close_door_cmd)-1)==0){
		// Set the close the door
		HAL_GPIO_WritePin(Door_GPIO_Port,Door_Pin,GPIO_PIN_SET);
		uart_driver_send(&uart_driver,(uint8_t *)ack_message, sizeof(ack_message)-1);

	}else if (memcmp(rx_packet, open_door_cmd, sizeof (open_door_cmd)-1)==0){
		// Set the open the door
		HAL_GPIO_WritePin(Door_GPIO_Port,Door_Pin,GPIO_PIN_RESET);
		uart_driver_send(&uart_driver,(uint8_t *)ack_message, sizeof(ack_message)-1);

	}else if (memcmp(rx_packet, get_door_position_cmd, sizeof (get_door_position_cmd)-1)==0){
		//get the door position
		if(HAL_GPIO_ReadPin(Door_GPIO_Port,Door_Pin)==1){

			const uint8_t close[] = "*D1#\r\n";
			uart_driver_send(&uart_driver,(uint8_t *)close, sizeof(close)-1);

		}else if(HAL_GPIO_ReadPin(Door_GPIO_Port,Door_Pin) == 0){
			const uint8_t open[] = "*D0#\r\n";
			uart_driver_send(&uart_driver,(uint8_t *)open, sizeof(open)-1);
		}


	}else if (memcmp(rx_packet, get_fw_version_cmd, sizeof (get_fw_version_cmd)-1)==0){
		// get the FW version
		uart_driver_send(&uart_driver,(uint8_t *)FW_VERSION, sizeof(FW_VERSION)-1);

	}else if (memcmp(rx_packet, get_unit_tick_cmd, sizeof (get_unit_tick_cmd)-1)==0){
		// get the tick of the system


	}else if (memcmp(rx_packet, get_heater_state_cmd, sizeof (get_heater_state_cmd)-1)==0){
		// get the status of the heater
		if(HAL_GPIO_ReadPin(Heater_GPIO_Port, Heater_Pin) == 1){
			uint8_t ON[] = "*H1#\r\n";
			uart_driver_send(&uart_driver,(uint8_t *)ON, sizeof(ON)-1);
		} else if(HAL_GPIO_ReadPin(Heater_GPIO_Port, Heater_Pin) == 0){
			uint8_t OFF[] = "*H0#\r\n";
			uart_driver_send(&uart_driver,(uint8_t *)OFF, sizeof(OFF)-1);
		}


	}else {
		uart_driver_send(&uart_driver, (uint8_t *)nack_message, sizeof(nack_message)-1);

	}if ((HAL_GetTick() - tick) > 1000) {
		 tick = HAL_GetTick();				// Evaluate the period
			char  buffer[23]={0};

			///problems woth the read of the temperature meas.
			// Read the temperature value
		       /*char buffer1[5];
			   uart_driver_send(&uart_driver,(uint8_t *) PROTOCOL_PREAMBLET, sizeof(PROTOCOL_PREAMBLET)-1);
			   float temp = temp_meas(1,1);
			   sprintf(buffer1,"%.2f",temp);
			   uart_driver_send(&uart_driver,(uint8_t *) read_temperature_cmd , sizeof( read_temperature_cmd)-1);*/
			   strcat(buffer,temp_probe);

			//door state
			if(HAL_GPIO_ReadPin(Door_GPIO_Port,Door_Pin)==1){
				const uint8_t close1[] = "*D1#";
				strcat(buffer,close1);

			}else if(HAL_GPIO_ReadPin(Door_GPIO_Port,Door_Pin) == 0){
				const uint8_t open1[] = "*D0#";
				strcat(buffer,open1);
			}


			//heater state
			if(HAL_GPIO_ReadPin(Heater_GPIO_Port, Heater_Pin) == 1){
				uint8_t ON1[] = "*H1#";
				strcat(buffer,ON1);

			} else if(HAL_GPIO_ReadPin(Heater_GPIO_Port, Heater_Pin) == 0){
				uint8_t OFF1[] = "*H0#";
				strcat(buffer,OFF1);
			}

			//Fan speed
			const uint8_t Zero_per1[] 		 =  "*F000#";
			const uint8_t twenty_five_per1[]  =  "*F025#";
			const uint8_t fifty_per1[] 		 =  "*F050#";
			const uint8_t seventy_five_per1[] =  "*F075#";
			const uint8_t one_hundred_per1[]  =  "*F100#";

			switch(state1){
					case 0:
						strcat(buffer,Zero_per1);
						break;
					case 1:
						strcat(buffer,twenty_five_per1);
						break;
					case 2:
						strcat(buffer,fifty_per1);
						break;
					case 3:
						strcat(buffer,seventy_five_per1);
						break;
					case 4:
						strcat(buffer,one_hundred_per1);
						state1 = 0;
						break;
					default:
						break;
					}
		HAL_UART_Transmit(&huart3, &buffer, sizeof(buffer), 10);
	}
}
