Final Project: Temperature Control of Cold Room Implementing an MQTT communication with the ESP32 

Author.
Jeronimo Jaramillo Bejarano
email: jeronimojaramillo.99@gmail.com

Objective. 

Design a system that should measure the temperature, and take the control of the actuators: the fan and the heater.
After, show the parameter on the display LCD. Through the UART protocol, the STM sends the character string of the parameters 
to the ESP32, and this device sends the data in the JSON structure using the MQTT protocol to send the data to the AWS server and publish
the information on the web page. 

PLATFORM.
The board STM32F429ZI discovery 1 and the ESP32 will be used on this development. 

FUNCTIONALITY.

* The STM has implemented the next command Parser: 

           Command		 response command
-->	*READ_TEMPERATURE#		*Txx.yy#
-->	*READ_FAN_SPEED#			*Fxyz#
-->	*SET_FAN_SPEED#			*ACK#
-->	*CLOSE_DOOR#			*ACK#
-->	*OPEN_DOOR#				*ACK#
-->	*GET_DOOR_POSITION#		*Dx#
-->	*GET_FW_VERSION#			*V 1.0.20220617#
-->	*GET_UNIT_TICK#			*CXX:YY:ZZ:HH#
--> 	*GET_HEATER_STATUS#		*Hx#

This command parser is implemented using the UART1 of the STM Boart to set and get the parameters of the system. 
each second in another part of the code was concatenated with the parameters in a string chain and uploaded to the ESP32 using the UART3 of
STM. In the part of the implementation of the ESP32. When the "Update" button on the web page was pushed, the information from the ESP32 was uploaded to the web.
When the "Firmware" button on the web page was pushed, the Firmware version from the ESP32 was uploaded to the web.


 